class AddFieldsToEpisode < ActiveRecord::Migration[7.0]
  def change
    add_column :episodes, :guid, :text
    add_column :episodes, :title, :text
    add_column :episodes, :description, :text
    add_column :episodes, :audio, :text
  end
end
