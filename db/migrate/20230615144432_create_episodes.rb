class CreateEpisodes < ActiveRecord::Migration[7.0]
  def change
    create_table :episodes do |t|
      t.string :url
      t.references :series, null: false, foreign_key: true
      t.datetime :added_on

      t.timestamps
    end
  end
end
