class CreateSeries < ActiveRecord::Migration[7.0]
  def change
    create_table :series do |t|
      t.string :url
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
