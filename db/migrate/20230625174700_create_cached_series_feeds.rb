class CreateCachedSeriesFeeds < ActiveRecord::Migration[7.0]
  def change
    create_table :cached_series_feeds do |t|
      t.string :url
      t.string :feed

      t.timestamps
    end
  end
end
