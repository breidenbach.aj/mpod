class AddOrderingToSeries < ActiveRecord::Migration[7.0]
  def change
    add_column :series, :ordering, :integer
  end
end
