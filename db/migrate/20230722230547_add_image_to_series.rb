class AddImageToSeries < ActiveRecord::Migration[7.0]
  def change
    add_column :series, :image, :text
  end
end
