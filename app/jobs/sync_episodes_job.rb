require 'russ'

class SyncEpisodesJob < ApplicationJob
  queue_as :default

  def perform(series)
    series.feed.items.each do |item|
      guid = item.guid.value
      title = item.title
      description = item.description || item.content
      audio = item.enclosure.url
      added_on = item.pub_date

      if (episode = Episode.find_by(guid:))
        episode.title = title
        episode.description = description
        episode.audio = audio
      else
        episode = Episode.new(
          guid:,
          series_id: series.id,
          title:,
          added_on:,
          description:,
          audio:
        )
      end
      puts(episode.inspect)
      episode.save
    end
    # Do something later
  end
end
