# frozen_string_literal: true

require 'russ'
require 'open-uri'

class Series < ApplicationRecord
  has_many :episodes

  def feed
    return @feed if @feed

    URI.parse(url).open do |rss|
      charset = rss.charset { Encoding.default_external }
      rss.set_encoding(charset)
      content = rss.read
      @feed = Russ::Channel.from_s(content)
    end

    @feed
  end

  def self.from_url(url)
    result = Series.new(url:)

    result.update
  end

  def update
    @feed = nil

    puts feed.image.inspect
    self.title = feed.title
    self.image = feed.image.url
    self.description = feed.description

    save

    SyncEpisodesJob.perform_now(self)
  end

  def oldest_first?
    ordering == 1
  end
end
