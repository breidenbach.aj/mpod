class Series::EpisodesController < ApplicationController
  def show
    @series = Series.find(params[:series_id])

    @episode = @series.episodes.find do |item|
      id = params[:episode_id]
      item.guid.sub(%r{/+}, '/') == id
    end
  end
end
