# frozen_string_literal: true

require 'json/ext'

class SeriesController < ApplicationController
  def show
    @series = Series.find(params[:id])
    respond_to do |format|
      format.json do
        render json: @series.feed.to_json
      end
      format.html do
        render 'series/show'
      end
    end
  end

  def index
    @all_series = Series.all
  end

  def create
    Series.from_url(params[:url])

    redirect_to action: :index
  end

  def update
    series = Series.find(params[:id])

    series.update

    redirect_to action: :show
  end

  def update_ordering
    @series = Series.find(params[:id])

    new_ordering = params[:ordering]

    @series.ordering = new_ordering
    @series.save

    redirect_to action: :show
  end

  def delete
    series = Series.find(id: params.id)
    series.destroy
  end
end
