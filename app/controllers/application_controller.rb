class ApplicationController < ActionController::Base
  def route_name
    (Rails.application.routes.recognize_path request.fullpath)[:controller]
  end

  def proxy_image(url, width: 240, height: 240)
    return unless url

    Imgproxy.url_for(
      url,
      width:,
      height:
    )
  end

  helper_method :route_name
  helper_method :proxy_image
end
