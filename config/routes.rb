Rails.application.routes.draw do
  get '/', to: 'series#index'
  namespace :series do
    get ':series_id/episodes/*episode_id', to: 'episodes#show', as: 'show_episode'
  end
  get 'series/:id', to: 'series#show', as: 'show_series'
  get 'series/', to: 'series#index', as: 'series'
  post 'series/new/', to: 'series#create', as: 'create_series'
  post 'series/:id/ordering', to: 'series#update_ordering', as: 'update_ordering'
  post 'series/:id/update', to: 'series#update', as: 'update_series'

  delete 'series/:id', to: 'series#delete', as: 'delete_series'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
