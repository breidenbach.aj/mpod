require "test_helper"

class Series::EpisodesControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get series_episodes_show_url
    assert_response :success
  end
end
